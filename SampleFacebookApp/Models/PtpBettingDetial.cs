﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace SampleFacebookApp.Models
{
    [Serializable]
    public class PtpBettingDetial
    {
        //program info
        public string programstatus { get; set; }
        public string programscode { get; set; }
        public string programsmtp { get; set; }
        public string raceno { get; set; }
        public string isplayed { get; set; }
        
        //odd info
        public String[] oddlist { get; set; }
       
        //pool value info
        public string winpool { get; set; }
        public string placepool { get; set; }
        public string showpool { get; set; }
        public string fourkind { get; set; }
        public string strflush { get; set; }
        public string highpool { get; set; }
        public string exacta { get; set; }
        public string trifecta { get; set; }
        public string superfecta { get; set; }
        
        //player chip balance
        public string chipbalance { get; set; }
        public string coinbalance { get; set; }
        public string high_won { get; set; }
        public string hands_played { get; set; }
        public string trn_played { get; set; }
        public string trn_wins { get; set; }
        public string cur_level { get; set; }


        //result info
        public string[] betresult { get; set; }
        public string winpaid1 { get; set; }
        public string placepaid1 { get; set; }
        public string placepaid2 { get; set; }
        public string showpaid1 { get; set; }
        public string showpaid2 { get; set; }
        public string showpaid3 { get; set; }
        public string exactaorder { get; set; }
        public string exactaorder1paid { get; set; }
        public string exactaorder2 { get; set; }
        public string exactaorder2paid { get; set; }
        public string trifectaorder { get; set; }
        public string trifectaorder1paid { get; set; }
        public string trifectaorder2 { get; set; }
        public string trifectaorder2paid { get; set; }
        public string superfectaorder { get; set; }
        public string superfectaorder1paid { get; set; }
        public string superfectaorder2 { get; set; }
        public string superfectaorder2paid { get; set; }
        public string superfectaorder3 { get; set; }
        public string superfectaorder3paid { get; set; }
        public string superfectaorder4 { get; set; }
        public string superfectaorder4paid { get; set; }
        
        //user info
        public string profilepic { get; set; }
        public string name { get; set; }
        public string tournament_win { get; set; }
        public string tournament_played { get; set; }
        public string highest_chip { get; set; }
        public string coins { get; set; }
        public ArrayList achivements { get; set; }

        //leader baord info
        public string ldr_name { get; set; }
        public string ldr_hchips { get; set; }
        public string ldr_nwins { get; set; }
        public string ldr_nwager { get; set; }
        public string ldr_ntourna { get; set; }
       
      // payoffs    

        public int payoffsexhand { get; set; }

        public string[] payoffsexhandval { get; set; }


    //user result info

        public String resultstatus { get; set; }
        public ArrayList hand_chosen { get; set; }
        public ArrayList bet_type { get; set; }
        public ArrayList amount_placed { get; set; }
        public ArrayList amount_won { get; set; }
        public ArrayList serialnumbers { get; set; }
        public ArrayList betstatus { get; set; }
        public int total { get; set; }
        public String totalchipwin { get; set; }
        public String totalcoinwin { get; set; }
        public string bettotal { get; set; }
        
    }
}