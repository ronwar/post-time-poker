﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SampleFacebookApp.Models
{
    public class FacebookUser
    {
        
    [Key] public string FacebookId { get; set; }
        public string Name { get; set; }
        public string EmailId { get; set; }
    }
}