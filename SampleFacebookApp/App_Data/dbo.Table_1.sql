﻿CREATE TABLE [dbo].[PTP_Facebook_Player]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Facebook_id] NUMERIC NOT NULL, 
    [Name] VARCHAR(50) NOT NULL
)
