﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Mvc.Facebook;
using Microsoft.AspNet.Mvc.Facebook.Client;
using SampleFacebookApp.Models;

using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Web;


namespace SampleFacebookApp.Controllers
{
    [Serializable]
    public class HomeController : Controller
    {

     //   PtpGameEntities ptpge = new PtpGameEntities();

        [FacebookAuthorize("email", "publish_actions", "user_games_activity")]
        public async Task<ActionResult> Index(FacebookContext context)
        {


            if (ModelState.IsValid)
            {

                var userinfo = await context.Client.GetCurrentUserAsync<MyAppUser>();
                HttpCookie appCookie = new HttpCookie("fbid");
                Session["fbid"] = userinfo.Id;
                Session["myappuser"] = userinfo;

                var conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ptpcon"].ConnectionString);
                conn.Open();

                string queryString = "SELECT Facebook_id FROM dbo.PTP_Facebook_Player where Facebook_id =" + userinfo.Id;
                SqlCommand command = new SqlCommand(queryString, conn);
                SqlDataReader reader = command.ExecuteReader();
                command.Dispose();
                if (!reader.Read())
                {
                    reader.Close();
                    command.CommandText = @"INSERT INTO PTP_Facebook_Player(Facebook_id,Name,Email,Create_date,Last_played_date,Total_chip_count,No_of_invites,No_of_shares,highest_chip_win,number_of_wagers,number_of_tournaments,no_of_wins,total_coin_count,current_level,no_of_ex_wins) 
                                        values (@fbid, @name, @email, @crd, @lpd, @tcc, @nv, @ns, @hc, @nwager, @nt, @nwins,@total_coin_count,@cl,@nxw)";

                    command.Parameters.Add("@fbid", SqlDbType.NVarChar, 50).Value = userinfo.Id;
                    command.Parameters.Add("@name", SqlDbType.NVarChar, 50).Value = userinfo.Name;
                    command.Parameters.Add("@email", SqlDbType.NVarChar, 50).Value = userinfo.Email;
                    command.Parameters.Add("@crd", SqlDbType.DateTime).Value = DateTime.Now;
                    command.Parameters.Add("@lpd", SqlDbType.DateTime).Value = DateTime.Now;
                    command.Parameters.Add("@tcc", SqlDbType.Decimal).Value = System.Convert.ToDecimal("5000");
                    command.Parameters.Add("@nv", SqlDbType.Decimal).Value = System.Convert.ToDecimal("0");
                    command.Parameters.Add("@ns", SqlDbType.Decimal).Value = System.Convert.ToDecimal("0");
                    command.Parameters.Add("@hc", SqlDbType.Decimal).Value = System.Convert.ToDecimal("0");
                    command.Parameters.Add("@nwager", SqlDbType.Decimal).Value = System.Convert.ToDecimal("0");
                    command.Parameters.Add("@nt", SqlDbType.Decimal).Value = System.Convert.ToDecimal("0");
                    command.Parameters.Add("@nwins", SqlDbType.Decimal).Value = System.Convert.ToDecimal("0");
                    command.Parameters.Add("@total_coin_count", SqlDbType.Decimal).Value = System.Convert.ToDecimal("0");
                    command.Parameters.Add("@cl", SqlDbType.Decimal).Value = 1;
                    command.Parameters.Add("@nxw", SqlDbType.Decimal).Value = 0;
                    command.ExecuteNonQuery();

                }

               
                    reader.Close();
                    
                    
                
               // var status = ptpge.fbUser.Find(userinfo.Id);
                var user = await context.Client.GetCurrentUserAsync<MyAppUser>();
                var friendsWithUpcomingBirthdays = user.Friends.Data.OrderBy(friend =>
                {
                    try
                    {
                        string friendBirthDayString = friend.Birthday;
                        if (String.IsNullOrEmpty(friendBirthDayString))
                        {
                            return int.MaxValue;
                        }

                        var birthDate = DateTime.Parse(friendBirthDayString);
                        friend.Birthday = birthDate.ToString("MMMM d"); // normalize birthday formats
                        return BirthdayCalculator.GetDaysBeforeBirthday(birthDate);
                    }
                    catch
                    {
                        return int.MaxValue;
                    }
                }).Take(100);
                conn.Close();

                user.Friends.Data = friendsWithUpcomingBirthdays.ToList();


                /* spin condition*/

           conn.Open();

     queryString = "SELECT Last_played_date FROM dbo.PTP_Facebook_Player where Facebook_id =" +userinfo.Id;
     command = new SqlCommand(queryString, conn);

    SqlDataReader sdr = command.ExecuteReader();
    if (sdr.Read())
    {

          String ldate = Convert.ToDateTime(sdr["Last_played_date"]).ToString("yyyy/MM/dd");
          String cdate  = DateTime.Now.ToString("yyyy/MM/dd");
          sdr.Close();
        if(ldate != cdate)
        {
            SqlCommand command2 = new SqlCommand(queryString, conn);
            command2.CommandText = "UPDATE dbo.PTP_Facebook_Player SET [Last_played_date]=@lpd WHERE Facebook_id =" + userinfo.Id;
            command2.Parameters.Add("@lpd", SqlDbType.DateTime).Value = DateTime.Now;
            command2.ExecuteNonQuery();
            conn.Close();
            return View("newvisit",user);
        }

        else
        {
          return View(user);
         //return View("newvisit", user);
        }
        
    }  
      
}

            return View("Error");  
}

       

        public ActionResult About()
        {
            return View();
        }


        // This action will handle the redirects from FacebookAuthorizeFilter when
        // the app doesn't have all the required permissions specified in the FacebookAuthorizeAttribute.
        // The path to this action is defined under appSettings (in Web.config) with the key 'Facebook:AuthorizationRedirectPath'.
        public ActionResult Permissions(FacebookRedirectContext context)
        {
            if (ModelState.IsValid)
            {
                return View(context);
            }

            return View("Error");
        }
    }
}