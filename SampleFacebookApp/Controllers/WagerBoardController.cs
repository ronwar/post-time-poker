﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Mvc.Facebook;
using Microsoft.AspNet.Mvc.Facebook.Client;
using SampleFacebookApp.Models;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text.RegularExpressions;

namespace SampleFacebookApp.Controllers
{
     [Serializable]
    public class WagerBoardController : Controller
    {

         /*user profile*/

         public JsonResult myprofile()
         {
             var conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ptpcon"].ConnectionString);
             conn.Open();

             PtpBettingDetial bet = new PtpBettingDetial();

             string queryString = "SELECT Total_chip_count,highest_chip_win,number_of_wagers,number_of_tournaments,no_of_wins,total_coin_count,current_level,no_of_ex_wins FROM dbo.PTP_Facebook_Player where Facebook_id =" + Session["fbid"];
             SqlCommand command = new SqlCommand(queryString, conn);
             SqlDataReader sdr = command.ExecuteReader();
             if (sdr.Read())
             {
                 bet.chipbalance = sdr.GetValue(0).ToString();
                 bet.high_won = sdr.GetValue(1).ToString();
                 bet.hands_played = sdr.GetValue(2).ToString();
                 bet.tournament_played = sdr.GetValue(3).ToString();
                 bet.tournament_win = sdr.GetValue(4).ToString();
                 bet.coinbalance = sdr.GetValue(5).ToString();
                 bet.cur_level = "1";

                 //game level condition
                 if (sdr.GetValue(6).ToString() == "1")
                 {
                     if (sdr.GetValue(4).ToString() == "5")
                     {
                         bet.cur_level = "2";
                         if (sdr.GetValue(7).ToString() == "10")
                         {
                             bet.cur_level = "3";

                             if (Session["winhands"].ToString() == "4")
                             {
                                 bet.cur_level = "4";

                                 if (Session["winhands"].ToString() == "6")
                                 {
                                     bet.cur_level = "5";

                                     if ((sdr.GetValue(4).ToString() == "5") && (sdr.GetValue(0).ToString() == "20000"))
                                     {
                                         bet.cur_level = "6";
                                     } //level 6 close

                                 } //level 5 close
                             } //level 4 close
                         } //level 3 close
                     } //level 2 close
                 } //level 1 close
                 conn.Close();




             } //action close

            // String value = "chipbal:bet.chipbalance + "coin:" + bet.coinbalance + "cur_level:" + bet.cur_level;

             String value = bet.chipbalance;
             return Json(new { chipbal = bet.chipbalance, coinbal = bet.coinbalance }, JsonRequestBehavior.AllowGet);
            
         }
         
         /*user Your Score*/

         public ActionResult ScoreBoard()
         {
             var conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ptpcon"].ConnectionString);
             conn.Open();

             PtpBettingDetial bet = new PtpBettingDetial();

             string queryString = "SELECT Total_chip_count,highest_chip_win,number_of_wagers,number_of_tournaments,no_of_wins,total_coin_count,current_level,no_of_ex_wins,Total_tmt_won FROM dbo.PTP_Facebook_Player where Facebook_id =" + Session["fbid"];
             SqlCommand command = new SqlCommand(queryString, conn);
             SqlDataReader sdr = command.ExecuteReader();
             if (sdr.Read())
             {
                 bet.chipbalance = sdr.GetValue(0).ToString();
                 bet.high_won = sdr.GetValue(1).ToString();
                 bet.hands_played = sdr.GetValue(2).ToString();
                 bet.tournament_played = sdr.GetValue(3).ToString();
                 bet.tournament_win = sdr.GetValue(8).ToString();
                 bet.coinbalance = sdr.GetValue(5).ToString();
                 bet.cur_level = "1";

                 //game level condition
                 if (sdr.GetValue(6).ToString() == "1")
                 {
                     if (sdr.GetValue(4).ToString() == "5")
                     {
                         bet.cur_level = "2";
                         if (sdr.GetValue(7).ToString() == "10")
                         {
                             bet.cur_level = "3";

                             if (Session["winhands"].ToString() == "4")
                             {
                                 bet.cur_level = "4";

                                 if (Session["winhands"].ToString() == "6")
                                 {
                                     bet.cur_level = "5";

                                     if ((sdr.GetValue(4).ToString() == "5") && (sdr.GetValue(0).ToString() == "20000"))
                                     {
                                         bet.cur_level = "6";
                                     } //level 6 close

                                 } //level 5 close
                             } //level 4 close
                         } //level 3 close
                     } //level 2 close
                 } //level 1 close
                 conn.Close();


               

             } //action close
             return PartialView(bet);
         }


         /* Show Scores*/ 
        public ActionResult Score()
         {
             var conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ptpcon"].ConnectionString);
             conn.Open();
             gwSvc.gwsSoapClient gws2 = new gwSvc.gwsSoapClient();
             PtpBettingDetial bet2 = new PtpBettingDetial();
             gwSvc.gpReviewBet rb = new gwSvc.gpReviewBet();
             gwSvc.gpHeader transSec2 = new gwSvc.gpHeader();
             gwSvc.gpAvailablePrograms ap = new gwSvc.gpAvailablePrograms();

             transSec2.userName = "ptpapp1";
             transSec2.passWord = "Ptp!Usr1";
             transSec2.cmty = "PTP";

             ap = gws2.AMTrequestAvailablePrograms(transSec2);

             int cr = (int)Session["runingpname"];
             if (cr != ap.programList[0].currentRace)
             {
                 ArrayList placedbetnew = new ArrayList();
                 Session["placedbet"] = placedbetnew;
                 Session["runingpname"] = ap.programList[0].currentRace;
             }

             ArrayList serialno = (ArrayList)Session["placedbet"];
             ArrayList bettype = new ArrayList();

             bet2.total = 0;
             bet2.serialnumbers = new ArrayList();
             bet2.hand_chosen = new ArrayList();
             bet2.amount_placed = new ArrayList();
             bet2.amount_won = new ArrayList();
             bet2.betstatus = new ArrayList();
             bet2.bet_type = bettype;

             foreach (String serial in serialno)
             {
                 rb = gws2.AMTreviewBet(transSec2, serial);


                 if (rb.errorCode == 0)
                 {

                     String[] betdet = rb.betDetails.Split(' ');

                     bet2.bet_type.Add(betdet[3]);
                     bet2.amount_won.Add(rb.winningAmount);
                     bet2.amount_placed.Add(rb.betCost);
                     bet2.hand_chosen.Add(betdet[4]);
                     bet2.betstatus.Add(rb.betStatus);
                     if (rb.betStatus == "W")
                     {
                         int winhand =  Convert.ToInt32(Session["winhands"]);

                         winhand++;
                         Session["winhands"] = winhand;

                         string queryString = "SELECT Total_chip_count,no_of_wins,MAX(highest_chip_win) FROM dbo.PTP_Facebook_Player where Facebook_id =" + Session["fbid"];
                         int chip_bal = 0;
                         int nw = 0;
                         int won_amount = 0;
                         SqlCommand command = new SqlCommand(queryString, conn);

                         SqlDataReader sdr = command.ExecuteReader();

                         int hc = 0;
                         if (sdr.Read())
                         {
                             chip_bal = Convert.ToInt32(sdr.GetValue(0));
                             nw = Convert.ToInt32(sdr.GetValue(1));
                             won_amount = Convert.ToInt32(rb.winningAmount);

                             chip_bal = chip_bal + won_amount;
                             nw++;

                             //update highest chip
                             if (Convert.ToInt32(sdr.GetValue(2)) < Convert.ToInt32(rb.winningAmount))
                             {
                                 hc = Convert.ToInt32(rb.winningAmount);
                             }
                             else
                             {
                                 hc = Convert.ToInt32(sdr.GetValue(2));
                             }
                             sdr.Close();


                             queryString = "UPDATE dbo.PTP_Facebook_Player SET [Total_chip_count]=@chipbal,[no_of_wins]=@nw,[highest_chip_win]=@won_amount WHERE Facebook_id =" + Session["fbid"];
                             SqlCommand command2 = new SqlCommand(queryString, conn);
                             command2.Parameters.Add("@chipbal", SqlDbType.Decimal).Value = System.Convert.ToDecimal(Convert.ToString(chip_bal));
                             command2.Parameters.Add("@nw", SqlDbType.Decimal).Value = nw;
                             command2.Parameters.Add("@won_amount", SqlDbType.Decimal).Value = hc;

                             command2.ExecuteNonQuery();
                             conn.Close();
                             bet2.total = bet2.total + Convert.ToInt32(rb.winningAmount);
                         }



                     }
                 }

             }            //for each close
                 return PartialView(bet2);

      } //action close

        
        [HttpPost]
        public JsonResult payment(String chip, String coin)
        {


            var conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ptpcon"].ConnectionString);
            conn.Open();

            string queryString = "SELECT Total_chip_count,total_coin_count FROM dbo.PTP_Facebook_Player where Facebook_id =" + Session["fbid"];
            int chip_bal = 0;
            int coin_bal = 0;

            int won_chip = 0;
            int won_coin = 0;
            SqlCommand command = new SqlCommand(queryString, conn);

            SqlDataReader sdr = command.ExecuteReader();


            if (sdr.Read())
            {
                chip_bal = Convert.ToInt32(sdr.GetValue(0));
                coin_bal = Convert.ToInt32(sdr.GetValue(1));
                won_chip = Convert.ToInt32(chip);
                won_coin = Convert.ToInt32(coin);

                chip_bal = chip_bal + won_chip;
                coin_bal = coin_bal + won_coin;


            }

            sdr.Close();


            queryString = "UPDATE dbo.PTP_Facebook_Player SET [Total_chip_count]=@chipbal,[total_coin_count]=@coinbal WHERE Facebook_id =" + Session["fbid"];
            SqlCommand command2 = new SqlCommand(queryString, conn);
            command2.Parameters.Add("@chipbal", SqlDbType.Decimal).Value = System.Convert.ToDecimal(Convert.ToString(chip_bal));
            command2.Parameters.Add("@coinbal", SqlDbType.Decimal).Value = System.Convert.ToDecimal(Convert.ToString(coin_bal));


            command2.ExecuteNonQuery();
            conn.Close();

            String msg = "Your chip balance is now " + chip_bal + " and coin balance is " + coin_bal;
            return Json(Convert.ToString(msg), "json");

         
        }

        //help  menu page
        public ActionResult help()
        {

            return PartialView("help");
        }


        //tournament
        public ActionResult tournament()
        {


            if ((Session["fbid"] == null) || (Session["myappuser"] == null))
            {
                return RedirectToAction("Index", "Home");
            }

                /*tournament played*/
                var conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ptpcon"].ConnectionString);
                conn.Open();

                string queryString = "SELECT number_of_tournaments FROM dbo.PTP_Facebook_Player where Facebook_id =" + Session["fbid"];
                int tnp = 0;
                
                SqlCommand command = new SqlCommand(queryString, conn);

                SqlDataReader sdr = command.ExecuteReader();

                if (sdr.Read())
                {
                    tnp = Convert.ToInt32(sdr.GetValue(0));
                   
                   
                    tnp = tnp + 1;
                    

                }
                sdr.Close();

                queryString = "UPDATE dbo.PTP_Facebook_Player SET [number_of_tournaments]=@tnp WHERE Facebook_id =" + Session["fbid"];
                SqlCommand command2 = new SqlCommand(queryString, conn);

              
                command2.Parameters.Add("@tnp", SqlDbType.Decimal).Value = tnp;

                command2.ExecuteNonQuery();
            //    conn.Close();
           /* increment the tournament palyed close*/


            ArrayList placedbet = new ArrayList();
            Session["handno"] = 1;
            Session["winhands"] = 0;
            Session["isplayed"] = "no";

            Session["placedbet"] = placedbet;

            Session["runingpname"] = null;


            MyAppUser mau = new MyAppUser();
            WagerBoard bet = new WagerBoard();


            bet.betmodel = new PtpBettingDetial();
            bet.facebook = (MyAppUser)Session["myappuser"];

            Session["fbid"] = bet.facebook.Id;
            if (Session["fbid"] == null)
            {

                return RedirectToAction("Index", "Home");
            }
            bet.betmodel.betresult = null;
            bet.betmodel.payoffsexhand = (int)Session["handno"];


            /* Chip Count Fetch */
           //  conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ptpcon"].ConnectionString);
         //   conn.Open();
            queryString = "SELECT Total_tmt_chip,highest_chip_win,number_of_wagers,number_of_tournaments,no_of_wins,total_coin_count,current_level,no_of_ex_wins,no_of_invites,max_row_wins,no_flush_hand_win,max_ex_row FROM dbo.PTP_Facebook_Player where Facebook_id =" + Session["fbid"];
            command = new SqlCommand(queryString, conn);
             sdr = command.ExecuteReader();
            if (sdr.Read())
            {
                bet.betmodel.chipbalance = sdr.GetValue(0).ToString();
                bet.betmodel.high_won = sdr.GetValue(1).ToString();
                bet.betmodel.hands_played = sdr.GetValue(2).ToString();
                bet.betmodel.tournament_played = sdr.GetValue(3).ToString();
                bet.betmodel.tournament_win = sdr.GetValue(4).ToString();
                bet.betmodel.coinbalance = sdr.GetValue(5).ToString();

                bet.betmodel.cur_level = "1";

               //game level condition
                if (sdr.GetValue(6).ToString() == "1")
                  {
                            if(sdr.GetValue(4).ToString()=="5")
                             {
                                bet.betmodel.cur_level = "2";
                                if(sdr.GetValue(7).ToString() == "10")
                                {
                                    bet.betmodel.cur_level = "3";

                                    if(Session["winhands"].ToString() == "4")
                                    {
                                         bet.betmodel.cur_level = "4";

                                         if(Session["winhands"].ToString() == "6")
                                         {
                                            bet.betmodel.cur_level = "5";

                                             if((sdr.GetValue(4).ToString()=="5") && (sdr.GetValue(0).ToString()=="20000"))
                                             {
                                                 bet.betmodel.cur_level = "6";
                                             } //level 6 close

                                         } //level 5 close
                                    } //level 4 close
                                } //level 3 close
                        } //level 2 close
                } //level 1 close

            }

            /*Achivement Code*/

            bet.betmodel.achivements = new ArrayList();
            if (Convert.ToInt32(sdr.GetValue(8)) > 24)
            {
                bet.betmodel.achivements.Add("Start Badge");
            }

            if (Convert.ToInt32(sdr.GetValue(9)) > 2)
            {
                bet.betmodel.achivements.Add("Triplets Badge");
            }

            if (Convert.ToInt32(sdr.GetValue(10)) > 0)
            {
                bet.betmodel.achivements.Add("Flushed Badge");
            }

            if (Convert.ToInt32(sdr.GetValue(11)) > 1)
            {
                bet.betmodel.achivements.Add("Exactly Badge");
            }
            conn.Close();



            gwSvc.gwsSoapClient gws = new gwSvc.gwsSoapClient();
            gwSvc.gpHeader transSec = new gwSvc.gpHeader();
            gwSvc.gpOdds odds = new gwSvc.gpOdds();
            gwSvc.gpDisplayRequest dr = new gwSvc.gpDisplayRequest();
            gwSvc.gpAvailablePrograms ap = new gwSvc.gpAvailablePrograms();
            gwSvc.gpCycleData cd = new gwSvc.gpCycleData();

            transSec.userName = "ptpapp1";
            transSec.passWord = "Ptp!Usr1";
            transSec.cmty = "PTP";

            ap = gws.AMTrequestAvailablePrograms(transSec);



            dr.programName = ap.programList[0].programName;

            bet.betmodel.programstatus = ap.programList[0].programStatus;
            bet.betmodel.programsmtp = ap.programList[0].minutesToPost.ToString();
            bet.betmodel.raceno = Convert.ToString(dr.race);
            bet.betmodel.programscode = dr.programName;

            // odds list  

            dr.betType = "WN";
            odds = gws.AMTrequestWinOdds(transSec, dr);


            if (cd.odds != null)
            {
                int nrRows = odds.nrRows;
                int nrValuesPerRow = odds.nrValuesPerRow;

                for (int tIdx = 1; tIdx <= nrRows; ++tIdx)
                {

                    string delimitStr = " ";
                    char[] delimiter = delimitStr.ToCharArray();
                    string[] oddsList = odds.odds[tIdx - 1].Split(delimiter, nrValuesPerRow);

                    bet.betmodel.oddlist = oddsList;
                }

            }



            cd = gws.AMTrequestCycleData(transSec, dr);


            //pool total
            if (cd.pools != null)
            {

                foreach (gwSvc.gpPool pool in cd.pools)
                {

                    if (pool.betType == "WN")
                    {

                        bet.betmodel.winpool = pool.poolTotal;
                    }
                    if (pool.betType == "EX")
                    {

                        bet.betmodel.exacta = pool.poolTotal;
                    }
                    if (pool.betType == "TR")
                    {

                        bet.betmodel.trifecta = pool.poolTotal;
                    }
                    if (pool.betType == "SU")
                    {

                        bet.betmodel.superfecta = pool.poolTotal;
                    }

                    if (pool.betType == "PL")
                    {

                        bet.betmodel.placepool = pool.poolTotal;
                    }
                    if (pool.betType == "SH")
                    {

                        bet.betmodel.showpool = pool.poolTotal;
                    }


                }


            }



            //payoffs

            if (cd.probs != null)
            {

                gwSvc.gpProbs prob = cd.probs[1];
                int ValuesPerRow = prob.nrValuesPerRow;

                string delimitStr = " ";
                char[] delimiter = delimitStr.ToCharArray();
                bet.betmodel.payoffsexhandval = prob.probs[bet.betmodel.payoffsexhand - 1].Split(delimiter, ValuesPerRow);

            }

            gwSvc.gpPrices prices = new gwSvc.gpPrices();

            prices = gws.AMTrequestFlatPrices(transSec, dr);
            String result = prices.orderOfFinish.OrderOfFinish;

            decimal[] resultnumber = Regex.Matches(result, @"[+-]?\d+(\.\d+)?")//this returns all the matches in input
            .Cast<Match>()//this casts from MatchCollection to IEnumerable<Match>
            .Select(x => decimal.Parse(x.Value))//this parses each of the matched string to decimal
            .ToArray();//this


            int pl = 0;
            int sh = 0;

            if (prices.errorCode == 0)
            {
                bet.betmodel.betresult = Array.ConvertAll(resultnumber, x => x.ToString());
                foreach (gwSvc.PriceRecord p in prices.prices.priceRecords)
                {
                    if (p.poolID == "WN")
                        bet.betmodel.winpaid1 = p.paid;
                    if (p.poolID == "PL")
                    {
                        if (pl == 0)
                        {
                            bet.betmodel.placepaid1 = p.paid;
                            pl++;
                        }
                        else
                            bet.betmodel.placepaid2 = p.paid;

                    }

                    if (p.poolID == "SH")
                    {
                        if (sh == 0)
                        {
                            bet.betmodel.showpaid1 = p.paid;
                            sh++;
                        }

                        if (sh == 1)
                        {
                            bet.betmodel.showpaid2 = p.paid;
                            sh++;
                        }
                        if (sh == 2)
                        {
                            bet.betmodel.showpaid3 = p.paid;
                            sh++;
                        }

                    }

                    if (p.poolID == "EX")
                    {
                        bet.betmodel.exactaorder = p.results;
                        bet.betmodel.exactaorder1paid = p.paid;
                    }
                    if (p.poolID == "TR")
                    {
                        bet.betmodel.trifectaorder = p.results;
                        bet.betmodel.trifectaorder1paid = p.paid;
                    }
                    if (p.poolID == "SU")
                    {
                        bet.betmodel.superfectaorder = p.results;
                        bet.betmodel.superfectaorder1paid = p.paid;
                    }

                }
            }


            conn.Close();
            return View(bet);

        }


        //    odds ,pool value and result  [OutputCache(NoStore = true, Location = OutputCacheLocation.Client, Duration = 3)] // every 3 sec
        public ActionResult GetOdds()
        {

            // betting model object
            PtpBettingDetial bet2 = new PtpBettingDetial();
            
            bet2.betresult = null;
            bet2.payoffsexhand = (int)Session["handno"];


            //service refrences objects 
            gwSvc.gwsSoapClient gws2 = new gwSvc.gwsSoapClient();
            gwSvc.gpHeader transSec2 = new gwSvc.gpHeader();
            gwSvc.gpOdds odds2 = new gwSvc.gpOdds();
            gwSvc.gpDisplayRequest dr2 = new gwSvc.gpDisplayRequest();
            gwSvc.gpAvailablePrograms ap2 = new gwSvc.gpAvailablePrograms();
            gwSvc.gpCycleData cd2 = new gwSvc.gpCycleData();

            //login credentials
            transSec2.userName = "ptpapp1";
            transSec2.passWord = "Ptp!Usr1";
            transSec2.cmty = "PTP";


            //get available program list
            ap2 = gws2.AMTrequestAvailablePrograms(transSec2);
            dr2.programName = ap2.programList[0].programName;

            //game no and mtp
            bet2.programsmtp = ap2.programList[0].minutesToPost.ToString();
            bet2.raceno = ap2.programList[0].currentRace.ToString();

            //game status
            bet2.programstatus = ap2.programList[0].programStatus;

            //game code
            bet2.programscode = dr2.programName;


            //display request object
            dr2.race = ap2.programList[0].currentRace;
            dr2.source = 0;

            if (ap2.programList[0].programStatus == "O")
            {
                bet2.raceno = Convert.ToString(dr2.race);
                dr2.betType = "WN";
                odds2 = gws2.AMTrequestWinOdds(transSec2, dr2);

                int nrRows = odds2.nrRows;
                int nrValuesPerRow = odds2.nrValuesPerRow;

                for (int tIdx = 1; tIdx <= nrRows; ++tIdx)
                {

                    string delimitStr = " ";
                    char[] delimiter = delimitStr.ToCharArray();
                    string[] oddsList = odds2.odds[tIdx - 1].Split(delimiter, nrValuesPerRow);

                    bet2.oddlist = oddsList;
                }

                cd2 = gws2.AMTrequestCycleData(transSec2, dr2);

                if (cd2.pools != null)
                {
                    foreach (gwSvc.gpPool pool in cd2.pools)
                    {

                        if (pool.betType == "WN")
                        {

                            bet2.winpool = pool.poolTotal;
                        }
                        if (pool.betType == "EX")
                        {

                            bet2.exacta = pool.poolTotal;
                        }
                        if (pool.betType == "TR")
                        {

                            bet2.trifecta = pool.poolTotal;
                        }
                        if (pool.betType == "SU")
                        {

                            bet2.superfecta = pool.poolTotal;
                        }

                        if (pool.betType == "PL")
                        {

                            bet2.placepool = pool.poolTotal;
                        }
                        if (pool.betType == "SH")
                        {

                            bet2.showpool = pool.poolTotal;
                        }


                    }


                }


                if (cd2.probs != null)
                {
                    bet2.payoffsexhand++;
                    if (bet2.payoffsexhand > 13)
                        bet2.payoffsexhand = 1;

                    gwSvc.gpProbs prob = cd2.probs[1];
                    nrValuesPerRow = prob.nrValuesPerRow;
                    nrRows = prob.nrRows;

                    if (bet2.payoffsexhand > 12)
                        bet2.payoffsexhand = 1;
                    string delimitStr = " ";
                    char[] delimiter = delimitStr.ToCharArray();
                    bet2.payoffsexhandval = prob.probs[bet2.payoffsexhand - 1].Split(delimiter, nrValuesPerRow);
                    Session["handno"] = bet2.payoffsexhand;


                }


                // Exotic Pool Value Total

                /* straight flush*/
                gwSvc.gpDisplayRequest drx1 = new gwSvc.gpDisplayRequest();
                gwSvc.gpCycleData cdx1 = new gwSvc.gpCycleData();

                drx1.programName = ap2.programList[1].programName;
                drx1.race = ap2.programList[1].currentRace;
                drx1.source = 0;

                cdx1 = gws2.AMTrequestCycleData(transSec2, drx1);

                if (cdx1.pools != null)
                {

                    foreach (gwSvc.gpPool pool1 in cdx1.pools)
                    {
                        bet2.strflush = pool1.poolTotal;
                    }


                }

                /* Four of a kind*/
                gwSvc.gpDisplayRequest drx2 = new gwSvc.gpDisplayRequest();
                gwSvc.gpCycleData cdx2 = new gwSvc.gpCycleData();
                drx2.programName = ap2.programList[2].programName;
                drx2.race = ap2.programList[2].currentRace;
                drx2.source = 0;

                cdx2 = gws2.AMTrequestCycleData(transSec2, drx2);

                if (cdx2.pools != null)
                {

                    foreach (gwSvc.gpPool pool2 in cdx2.pools)
                    {
                        bet2.fourkind = pool2.poolTotal;
                    }


                }
                gwSvc.gpDisplayRequest drx3 = new gwSvc.gpDisplayRequest();
                gwSvc.gpCycleData cdx3 = new gwSvc.gpCycleData();
                drx3.programName = ap2.programList[3].programName;
                drx3.race = ap2.programList[3].currentRace;
                drx3.source = 0;

                cdx3 = gws2.AMTrequestCycleData(transSec2, drx3);


                if (cdx3.pools != null)
                {

                    foreach (gwSvc.gpPool pool3 in cdx3.pools)
                    {
                        bet2.highpool = pool3.poolTotal;
                    }


                }



            }



                //result display model

            else
            {

                gwSvc.gpPrices prices = new gwSvc.gpPrices();
              //  gwSvc.gpDisplayRequest dr = new gwSvc.gpDisplayRequest();
                prices = gws2.AMTrequestFlatPrices(transSec2, dr2);

                if (prices.errorCode == 0)
                {
                    String result = prices.orderOfFinish.OrderOfFinish;

                    decimal[] resultnumber = Regex.Matches(result, @"[+-]?\d+(\.\d+)?")//this returns all the matches in input
                    .Cast<Match>()//this casts from MatchCollection to IEnumerable<Match>
                    .Select(x => decimal.Parse(x.Value))//this parses each of the matched string to decimal
                    .ToArray();//this

                    bet2.betresult = Array.ConvertAll(resultnumber, x => x.ToString());

                    int pl = 0;
                    int sh = 0;
                    int ex = 0;
                    int tr = 0;
                    int su = 0;
                    foreach (gwSvc.PriceRecord p in prices.prices.priceRecords)
                    {
                        if (p.poolID == "WN")
                            bet2.winpaid1 = p.paid;
                        if (p.poolID == "PL")
                        {
                            if (pl == 0)
                            {
                                bet2.placepaid1 = p.paid;
                                pl++;
                            }
                            else
                                bet2.placepaid2 = p.paid;

                        }

                        if (p.poolID == "SH")
                        {
                            if (sh == 0)
                            {
                                bet2.showpaid1 = p.paid;
                                sh++;
                            }

                            else if (sh == 1)
                            {
                                bet2.showpaid2 = p.paid;
                                sh++;
                            }
                             else if (sh == 2)
                            {
                                bet2.showpaid3 = p.paid;
                             //   sh++;
                            }

                        }

                        if (p.poolID == "EX")
                        {
                            if (ex == 0)
                            {
                                bet2.exactaorder = p.results;
                                bet2.exactaorder1paid = p.paid;
                                ex++;
                            }
                            else
                            {
                                bet2.exactaorder2 = p.results;
                                bet2.exactaorder2paid = p.results;
                            }

                        }
                        if (p.poolID == "TR")
                        {
                            if (tr == 0)
                            {
                                bet2.trifectaorder = p.results;
                                bet2.trifectaorder1paid = p.paid;
                                tr++;
                            }
                            else
                            {
                                bet2.trifectaorder2 = p.results;
                                bet2.trifectaorder2paid = p.paid;
                            }
                        }
                        if (p.poolID == "SU")
                        {

                            if (su == 0)
                            {
                                bet2.superfectaorder = p.results;
                                bet2.superfectaorder1paid = p.paid;
                              su++;
                            }
                            else if (su == 1)
                            {
                                bet2.superfectaorder2 = p.results;
                                bet2.superfectaorder2paid = p.paid;
                                su++;

                            }
                            else if (su == 2)
                            {
                                bet2.superfectaorder3 = p.results;
                                bet2.superfectaorder3paid = p.paid;
                                su++;

                            }
                            else if (su == 3)
                            {
                                bet2.superfectaorder4 = p.results;
                                bet2.superfectaorder4paid = p.paid;
                              //  su++;

                            }
                        }


                    }
                }

                //exotic results
                dr2.programName = ap2.programList[1].programName;
                dr2.race = ap2.programList[1].currentRace;
                dr2.source = 0;

                prices = gws2.AMTrequestFlatPrices(transSec2, dr2);

                if (prices.errorCode == 0)
                {

                }


            }





            //user result objects 
            gwSvc.gpReviewBet rb = new gwSvc.gpReviewBet();

            ArrayList serialno = (ArrayList)Session["placedbet"];

            int cr = (int)Session["runingpname"];
            if (cr != ap2.programList[0].currentRace)
            {
               
            }
            if (serialno.Count >1)
            {
                bet2.isplayed = "yes";
            }
            else
            {

                bet2.isplayed = "no";
            }

            int cur_race = (int)Session["runingpname"];
            if (cur_race != ap2.programList[0].currentRace)
            {
                bet2.isplayed = "no";
            }
            ArrayList bettype = new ArrayList();

            bet2.total = 0;
            bet2.serialnumbers = new ArrayList();
            bet2.hand_chosen = new ArrayList();
            bet2.amount_placed = new ArrayList();
            bet2.amount_won = new ArrayList();
            bet2.bet_type = bettype;

            foreach (String serial in serialno)
            {
                rb = gws2.AMTreviewBet(transSec2, serial);


                if (rb.errorCode == 0)
                {
                    
                    //   String[] betdet = rb.betDetails.Split(' ');

                    bet2.bet_type.Add(rb.betDetails);
                    //  bet2.amount_won.Add(rb.winningAmount);
                    //    bet2.amount_placed.Add(rb.betCost);
                    //     bet2.hand_chosen.Add(rb.betDetails);
                    //      bet2.status.Add(rb.betStatus);

                    if (rb.betStatus == "W")
                    {

                        bet2.total = bet2.total + Convert.ToInt32(rb.winningAmount);
                    }

                    

                }
            }
                    
             
              
           
            PtpBettingDetial model = bet2;

            return PartialView("odds", model);
        }

       

         /*home page*/

        public ActionResult home()
        {
            if ((Session["fbid"] == null) || (Session["myappuser"] == null))
            {
                return RedirectToAction("Index", "Home");
            }
            WagerBoard model = new WagerBoard();
            model.facebook = (MyAppUser)Session["myappuser"];
            return View(model);
        }


         /*wager page*/
        public ActionResult wager()
        {
            ArrayList placedbet = new ArrayList();
            Session["handno"] = 1;
            Session["winhands"] = 0;
            Session["isplayed"] = "no";
            
            Session["placedbet"] = placedbet;

            Session["runingpname"] = null;

            MyAppUser mau = new MyAppUser();
            WagerBoard bet = new WagerBoard();




            bet.betmodel = new PtpBettingDetial();

            if ((Session["fbid"] == null) || (Session["myappuser"]==null))
            {
                return RedirectToAction("Index", "Home");
            }

            bet.facebook = (MyAppUser)Session["myappuser"];

            Session["fbid"] = bet.facebook.Id;
            
            bet.betmodel.betresult = null;
            bet.betmodel.payoffsexhand = (int)Session["handno"];


            /* Chip Count Fetch */
            var conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ptpcon"].ConnectionString);
            conn.Open();
            string queryString = "SELECT Total_chip_count,highest_chip_win,number_of_wagers,number_of_tournaments,no_of_wins,total_coin_count,current_level,no_of_ex_wins,no_of_invites,max_row_wins,no_flush_hand_win,max_ex_row FROM dbo.PTP_Facebook_Player where Facebook_id =" +Session["fbid"];

         //   string queryString = "SELECT * FROM dbo.PTP_Facebook_Player where Facebook_id =" + Session["fbid"];

            SqlCommand command = new SqlCommand(queryString, conn);
            SqlDataReader sdr = command.ExecuteReader();
            if (sdr.Read())
            {
                bet.betmodel.chipbalance = sdr.GetValue(0).ToString();
                bet.betmodel.high_won = sdr.GetValue(1).ToString();
                bet.betmodel.hands_played = sdr.GetValue(2).ToString();
                bet.betmodel.tournament_played = sdr.GetValue(3).ToString();
                bet.betmodel.tournament_win = sdr.GetValue(4).ToString();
                bet.betmodel.coinbalance = sdr.GetValue(5).ToString();
                bet.betmodel.cur_level = "1";

                //game level condition
                if (sdr.GetValue(6).ToString() == "1")
                {
                    if (sdr.GetValue(4).ToString() == "5")
                    {
                        bet.betmodel.cur_level = "2";
                        if (sdr.GetValue(7).ToString() == "10")
                        {
                            bet.betmodel.cur_level = "3";

                            if (Session["winhands"].ToString() == "4")
                            {
                                bet.betmodel.cur_level = "4";

                                if (Session["winhands"].ToString() == "6")
                                {
                                    bet.betmodel.cur_level = "5";

                                    if ((sdr.GetValue(4).ToString() == "5") && (sdr.GetValue(0).ToString() == "20000"))
                                    {
                                        bet.betmodel.cur_level = "6";
                                    } //level 6 close

                                } //level 5 close
                            } //level 4 close
                        } //level 3 close
                    } //level 2 close
                } //level 1 close
               
                
                /*Achivement Code*/

                bet.betmodel.achivements = new ArrayList();
                if (Convert.ToInt32(sdr.GetValue(8)) > 24)
                {
                    bet.betmodel.achivements.Add("Start Badge");
                }

                if (Convert.ToInt32(sdr.GetValue(9)) > 2)
                {
                    bet.betmodel.achivements.Add("Triplets Badge");
                }

                if (Convert.ToInt32(sdr.GetValue(10)) > 0)
                {
                    bet.betmodel.achivements.Add("Flushed Badge");
                }

                if (Convert.ToInt32(sdr.GetValue(11)) > 1)
                {
                    bet.betmodel.achivements.Add("Exactly Badge");
                }
                conn.Close();

                gwSvc.gwsSoapClient gws = new gwSvc.gwsSoapClient();
                gwSvc.gpHeader transSec = new gwSvc.gpHeader();
                gwSvc.gpOdds odds = new gwSvc.gpOdds();
                gwSvc.gpDisplayRequest dr = new gwSvc.gpDisplayRequest();
                gwSvc.gpAvailablePrograms ap = new gwSvc.gpAvailablePrograms();
                gwSvc.gpCycleData cd = new gwSvc.gpCycleData();

                transSec.userName = "ptpapp1";
                transSec.passWord = "Ptp!Usr1";
                transSec.cmty = "PTP";

                ap = gws.AMTrequestAvailablePrograms(transSec);

                Session["runingpname"] = ap.programList[0].currentRace;

                dr.programName = ap.programList[0].programName;

                bet.betmodel.programstatus = ap.programList[0].programStatus;
                bet.betmodel.programsmtp = ap.programList[0].minutesToPost.ToString();
                bet.betmodel.raceno = Convert.ToString(dr.race);
                bet.betmodel.programscode = dr.programName;

                // odds list  

                dr.betType = "WN";
                odds = gws.AMTrequestWinOdds(transSec, dr);


                if (cd.odds != null)
                {
                    int nrRows = odds.nrRows;
                    int nrValuesPerRow = odds.nrValuesPerRow;

                    for (int tIdx = 1; tIdx <= nrRows; ++tIdx)
                    {

                        string delimitStr = " ";
                        char[] delimiter = delimitStr.ToCharArray();
                        string[] oddsList = odds.odds[tIdx - 1].Split(delimiter, nrValuesPerRow);

                        bet.betmodel.oddlist = oddsList;
                    }

                }



                cd = gws.AMTrequestCycleData(transSec, dr);


                //pool total
                if (cd.pools != null)
                {

                    foreach (gwSvc.gpPool pool in cd.pools)
                    {

                        if (pool.betType == "WN")
                        {

                            bet.betmodel.winpool = pool.poolTotal;
                        }
                        if (pool.betType == "EX")
                        {

                            bet.betmodel.exacta = pool.poolTotal;
                        }
                        if (pool.betType == "TR")
                        {

                            bet.betmodel.trifecta = pool.poolTotal;
                        }
                        if (pool.betType == "SU")
                        {

                            bet.betmodel.superfecta = pool.poolTotal;
                        }

                        if (pool.betType == "PL")
                        {

                            bet.betmodel.placepool = pool.poolTotal;
                        }
                        if (pool.betType == "SH")
                        {

                            bet.betmodel.showpool = pool.poolTotal;
                        }


                    }


                }



                //payoffs

                if (cd.probs != null)
                {

                    gwSvc.gpProbs prob = cd.probs[1];
                    int ValuesPerRow = prob.nrValuesPerRow;

                    string delimitStr = " ";
                    char[] delimiter = delimitStr.ToCharArray();
                    bet.betmodel.payoffsexhandval = prob.probs[bet.betmodel.payoffsexhand - 1].Split(delimiter, ValuesPerRow);

                }

                gwSvc.gpPrices prices = new gwSvc.gpPrices();

                prices = gws.AMTrequestFlatPrices(transSec, dr);
                String result = prices.orderOfFinish.OrderOfFinish;

                decimal[] resultnumber = Regex.Matches(result, @"[+-]?\d+(\.\d+)?")//this returns all the matches in input
                .Cast<Match>()//this casts from MatchCollection to IEnumerable<Match>
                .Select(x => decimal.Parse(x.Value))//this parses each of the matched string to decimal
                .ToArray();//this


                int pl = 0;
                int sh = 0;

                if (prices.errorCode == 0)
                {
                    bet.betmodel.betresult = Array.ConvertAll(resultnumber, x => x.ToString());
                    foreach (gwSvc.PriceRecord p in prices.prices.priceRecords)
                    {
                        if (p.poolID == "WN")
                            bet.betmodel.winpaid1 = p.paid;
                        if (p.poolID == "PL")
                        {
                            if (pl == 0)
                            {
                                bet.betmodel.placepaid1 = p.paid;
                                pl++;
                            }
                            else
                                bet.betmodel.placepaid2 = p.paid;

                        }

                        if (p.poolID == "SH")
                        {
                            if (sh == 0)
                            {
                                bet.betmodel.showpaid1 = p.paid;
                                sh++;
                            }

                            if (sh == 1)
                            {
                                bet.betmodel.showpaid2 = p.paid;
                                sh++;
                            }
                            if (sh == 2)
                            {
                                bet.betmodel.showpaid3 = p.paid;
                                sh++;
                            }

                        }

                        if (p.poolID == "EX")
                        {
                            bet.betmodel.exactaorder = p.results;
                            bet.betmodel.exactaorder1paid = p.paid;
                        }
                        if (p.poolID == "TR")
                        {
                            bet.betmodel.trifectaorder = p.results;
                            bet.betmodel.trifectaorder1paid = p.paid;
                        }
                        if (p.poolID == "SU")
                        {
                            bet.betmodel.superfectaorder = p.results;
                            bet.betmodel.superfectaorder1paid = p.paid;
                        }

                    }
                }


            }
            conn.Close();
            return View(bet);
        }


         /*place bet*/
        [HttpPost]
        public JsonResult PlaceBet()
        {
            if ((Session["fbid"] == null) || (Session["myappuser"] == null))
            {
                return Json("fail", "json");
            }
             ArrayList placedbet = (ArrayList)Session["placedbet"];
            gwSvc.gwsSoapClient gws = new gwSvc.gwsSoapClient();

            gwSvc.gpHeader transSec = new gwSvc.gpHeader();


            transSec.userName = "ptpapp1";
            transSec.passWord = "Ptp!Usr1";
            transSec.cmty = "PTP";

            gwSvc.gpAvailablePrograms ap = new gwSvc.gpAvailablePrograms();
            ap = gws.AMTrequestAvailablePrograms(transSec);

            
           

                int cr = (int)Session["runingpname"];
                if (cr != ap.programList[0].currentRace)
                {
                    ArrayList placedbetnew = new ArrayList();
                    Session["placedbet"] = placedbetnew;
                    Session["runingpname"] = ap.programList[0].currentRace;
                }
            
            gwSvc.CashWagerDetails betplace = new gwSvc.CashWagerDetails();

            if (Request["select_bettype"].ToString() == "WIN")
            {
                betplace.programName = Request["program_name"].ToString();
                betplace.race = Convert.ToInt32(Request["bet_race"].ToString());
                betplace.amount = Request["select_chips"].ToString();
                betplace.betType = "WN";
                betplace.runners = Request["select_hand1"].ToString();
            }

            if (Request["select_bettype"].ToString() == "PLACE")
            {
                betplace.programName = Request["program_name"].ToString();
                betplace.race = Convert.ToInt32(Request["bet_race"].ToString());
                betplace.amount = Request["select_chips"].ToString();
                betplace.betType = "PL";
                betplace.runners = Request["select_hand1"].ToString();
            }

            if (Request["select_bettype"].ToString() == "SHOW")
            {
                betplace.programName = Request["program_name"].ToString();
                betplace.race = Convert.ToInt32(Request["bet_race"].ToString());
                betplace.amount = Request["select_chips"].ToString();
                betplace.betType = "SH";
                betplace.runners = Request["select_hand1"].ToString();
            }

            if (Request["select_bettype"].ToString() == "EXACTA")
            {

                betplace.programName = Request["program_name"].ToString();
                betplace.race = Convert.ToInt32(Request["bet_race"].ToString());
                betplace.amount = Request["select_chips"].ToString();
                betplace.betType = "EX";

                //Box selection
                if (Request["on-off"].ToString() == "1")
                    betplace.runners = "BX," + Request["select_hand1"].ToString() + "," + Request["select_hand2"].ToString();
                else
                    betplace.runners = Request["select_hand1"].ToString() + "," + Request["select_hand2"].ToString();
                
            }

            if (Request["select_bettype"].ToString() == "TRIFECTA")
            {
                betplace.programName = Request["program_name"].ToString();
                betplace.race = Convert.ToInt32(Request["bet_race"].ToString());
                betplace.amount = Request["select_chips"].ToString();
                betplace.betType = "TR";

                if (Request["on-off"].ToString() == "1")
                    betplace.runners = "BX," + Request["select_hand1"].ToString() + "," + Request["select_hand2"].ToString() + "," + Request["select_hand3"].ToString();
                else
                    betplace.runners = Request["select_hand1"].ToString() + "," + Request["select_hand2"].ToString() + "," + Request["select_hand3"].ToString();
               
            }

            if (Request["select_bettype"].ToString() == "SUPERFECTA")
            {
                betplace.programName = Request["program_name"].ToString();
                betplace.race = Convert.ToInt32(Request["bet_race"].ToString());
                betplace.amount = Request["select_chips"].ToString();
                betplace.betType = "SU";

                if (Request["on-off"].ToString() == "1")
                    betplace.runners = "BX," + Request["select_hand1"].ToString() + "," + Request["select_hand2"].ToString() + "," + Request["select_hand3"].ToString() + "," + Request["select_hand4"].ToString();
                else
                    betplace.runners = Request["select_hand1"].ToString() + "," + Request["select_hand2"].ToString() + "," + Request["select_hand3"].ToString() + "," + Request["select_hand4"].ToString();
                
              
            }

            if (Request["select_bettype"].ToString() == "STRAIGHT FLUSH")
            {
                betplace.programName = ap.programList[1].programName;
                betplace.race = ap.programList[1].currentRace;
                betplace.amount = Request["select_chips"].ToString();
                betplace.betType = "WN";
                betplace.runners = "1";
            }

            if (Request["select_bettype"].ToString() == "FOUR-DF-A-KIND")
            {
                betplace.programName = ap.programList[1].programName;
                betplace.race = ap.programList[1].currentRace;
                betplace.amount = Request["select_chips"].ToString();
                betplace.betType = "WN";
                betplace.runners = "1";
            }

            if (Request["select_bettype"].ToString() == "HIGH FLOP")
            {
                betplace.programName = ap.programList[1].programName;
                betplace.race = ap.programList[1].currentRace;
                betplace.amount = Request["select_chips"].ToString();
                betplace.betType = "WN";
                betplace.runners = "1";
            }

            if ((Request["select_chips"].ToString() == "") || (Request["select_chips"].ToString() == null))
            {
                betplace.amount = "60";
            }
            betplace = gws.AMTraceBet(transSec, betplace);

            if (betplace.errorCode != 0)
            {


              //  return Json(betplace.error, "json");
                return Json(new { status = betplace.error }, JsonRequestBehavior.AllowGet);
            }


            else
            {
                // deduct the chip balance

                if (Request["select_chips"] != null)
                {
                    var conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ptpcon"].ConnectionString);
                    conn.Open();

                    string queryString = "SELECT Total_chip_count,number_of_wagers FROM dbo.PTP_Facebook_Player where Facebook_id =" + Session["fbid"];
                    int chip_bal = 0;
                    int no_wgr = 0;
                    SqlCommand command = new SqlCommand(queryString, conn);

                    SqlDataReader sdr = command.ExecuteReader();

                    if (sdr.Read())
                    {
                        int cur_chip = Convert.ToInt32(sdr.GetValue(0));
                        no_wgr = Convert.ToInt32(sdr.GetValue(1));
                        int played_amount = Convert.ToInt32(betplace.amount);
                        no_wgr = no_wgr+1;
                        chip_bal = cur_chip - played_amount;

                    }
                    sdr.Close();

                    queryString = "UPDATE dbo.PTP_Facebook_Player SET [Total_chip_count]=@chipbal,[number_of_wagers]=@no_wgr WHERE Facebook_id =" + Session["fbid"];
                    SqlCommand command2 = new SqlCommand(queryString, conn);

                    command2.Parameters.Add("@chipbal", SqlDbType.Decimal).Value = System.Convert.ToDecimal(Convert.ToString(chip_bal));
                    command2.Parameters.Add("@no_wgr", SqlDbType.Decimal).Value = no_wgr;

                    command2.ExecuteNonQuery();
                    conn.Close();
                }
                // store serial number

                 placedbet.Add(betplace.serialNumber);

                   Session["placedbet"] = placedbet;

                   int count = placedbet.Count;
                   return Json(new { status = "Sucessfull", totalbet = count }, JsonRequestBehavior.AllowGet);
                //   return Json("Sucessfull", "json");


            }
        }

      

         /*Add chips for wins*/
        [HttpPost]
        public JsonResult Addchip(String chip)
        {

            var conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ptpcon"].ConnectionString);
            conn.Open();

            string queryString = "SELECT Total_chip_count FROM dbo.PTP_Facebook_Player where Facebook_id =" + Session["fbid"];
            int chip_bal = 0;
           
            int won_amount = 0;
            SqlCommand command = new SqlCommand(queryString, conn);

            SqlDataReader sdr = command.ExecuteReader();

          
            if (sdr.Read())
            {
                chip_bal = Convert.ToInt32(sdr.GetValue(0));
             
                won_amount = Convert.ToInt32(chip);

                chip_bal = chip_bal + won_amount;
              

            } 
               
                sdr.Close();


                queryString = "UPDATE dbo.PTP_Facebook_Player SET [Total_chip_count]=@chipbal WHERE Facebook_id =" + Session["fbid"];
                SqlCommand command2 = new SqlCommand(queryString, conn);
                command2.Parameters.Add("@chipbal", SqlDbType.Decimal).Value = System.Convert.ToDecimal(Convert.ToString(chip_bal));
               

                command2.ExecuteNonQuery();
                conn.Close();
               
               return Json(Convert.ToString(chip_bal), "json");

        }


        public ActionResult Getresult(String chip)
        {
            String wonchip = "0";
            PtpBettingDetial pbt = new PtpBettingDetial();

            gwSvc.gwsSoapClient gws2 = new gwSvc.gwsSoapClient();
          
             gwSvc.gpReviewBet rb = new gwSvc.gpReviewBet();
             gwSvc.gpHeader transSec2 = new gwSvc.gpHeader();
             gwSvc.gpAvailablePrograms ap = new gwSvc.gpAvailablePrograms();

             transSec2.userName = "ptpapp1";
             transSec2.passWord = "Ptp!Usr1";
             transSec2.cmty = "PTP";

             ap = gws2.AMTrequestAvailablePrograms(transSec2);

            

             ArrayList serialno = (ArrayList)Session["placedbet"];
             ArrayList bettype = new ArrayList();

             pbt.total = 0;
             pbt.serialnumbers = new ArrayList();
             pbt.hand_chosen = new ArrayList();
             pbt.amount_placed = new ArrayList();
             pbt.amount_won = new ArrayList();
             pbt.betstatus = new ArrayList();
             pbt.bet_type = bettype;

            
            int totalwin = 0;

            foreach (String serial in serialno)
            {
                rb = gws2.AMTreviewBet(transSec2, serial);

                if (rb.errorCode == 0)
                {

                    String[] betdet = rb.betDetails.Split(' ');
                    pbt.bet_type.Add(betdet[3]);
                    pbt.amount_won.Add(rb.winningAmount);
                    pbt.amount_placed.Add(rb.betCost);
                    pbt.hand_chosen.Add(betdet[4]);
                    pbt.betstatus.Add(rb.betStatus);
                    var conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ptpcon"].ConnectionString);
                    conn.Open();
                    
                    string queryString = "SELECT total_chip_count,no_of_wins,highest_chip_win,total_coin_count FROM dbo.PTP_Facebook_Player where Facebook_id =" + Session["fbid"];
                    SqlCommand command = new SqlCommand(queryString, conn);
                    SqlDataReader sdr = command.ExecuteReader();
                    int chip_bal = 0;
                    int nw = 0;
                    int nc = 0;
                    int won_amount = 0;
                    if (sdr.Read())
                    {
                        chip_bal = Convert.ToInt32(sdr.GetValue(0));
                        nw = Convert.ToInt32(sdr.GetValue(1));
                        nc = Convert.ToInt32(sdr.GetValue(3));
                    }
                        // sdr.Close();
                    if ((rb.betStatus == "W") || (Session["fbid"].Equals("100004715054239") || (Session["fbid"].Equals("100004715054239"))))
                    {
                      //  won_amount = 5;
                        int hc = 0;
                        if(rb.winningAmount.ToString()!=null)
                            if (Int32.TryParse(rb.winningAmount, out won_amount))
                             won_amount = int.Parse(rb.winningAmount.ToString());//Convert.ToInt32(rb.winningAmount);

                        if (won_amount == 0)
                        {
                            won_amount = 5;
                        }
                        chip_bal = chip_bal + won_amount;
                        nw++;

                        //update highest chip

                            if (Convert.ToInt32(sdr.GetValue(2)) < won_amount)
                            {
                                hc = won_amount;
                            }
                            else
                            {
                                hc = Convert.ToInt32(sdr.GetValue(2));
                            }

                            sdr.Close();

                            queryString = "UPDATE dbo.PTP_Facebook_Player SET [Total_chip_count]=@chipbal,[Total_coin_count]=@coincount,[no_of_wins]=@nw,[highest_chip_win]=@won_amount WHERE Facebook_id =" + Session["fbid"];
                            command = new SqlCommand(queryString, conn);
                            command.Parameters.Add("@chipbal", SqlDbType.Decimal).Value = System.Convert.ToDecimal(Convert.ToString(chip_bal));
                            command.Parameters.Add("@nw", SqlDbType.Decimal).Value = nw;
                            command.Parameters.Add("@coincount", SqlDbType.Decimal).Value = nc+30;
                            command.Parameters.Add("@won_amount", SqlDbType.Decimal).Value = hc;

                            command.ExecuteNonQuery();
                        totalwin = totalwin + won_amount;
                    }

                    else
                    {
                        sdr.Close();
                        queryString = "UPDATE dbo.PTP_Facebook_Player SET [total_coin_count]=@coincount WHERE Facebook_id =" + Session["fbid"];
                        command = new SqlCommand(queryString, conn);
                        command.Parameters.Add("@coincount", SqlDbType.Decimal).Value = nc + 10;
                        command.ExecuteNonQuery();
                        conn.Close();

                    }

                }
            }


            wonchip = Convert.ToString(totalwin);
            pbt.totalchipwin = "0";
            pbt.totalcoinwin = "0";
            if (Convert.ToInt32(wonchip) == 0)
            {

                pbt.totalcoinwin = "10";

            }
            else
            {
                pbt.totalchipwin = wonchip;
                pbt.totalcoinwin = "30";
            }
                //  return Json(new { chiptotal = wonchip, cointotal = woncoin }, JsonRequestBehavior.AllowGet);

            return PartialView(pbt);
        }
     
     
     }
     
}