	shuffle = function(o) {
		for ( var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x)			;
		return o;
	};
	String.prototype.hashCode = function(){		
		var hash = 5381;
		for (i = 0; i < this.length; i++) {
			char = this.charCodeAt(i);
			hash = ((hash<<5)+hash) + char;
			hash = hash & hash; // Convert to 32bit integer
		}
		return hash;
	}
	Number.prototype.mod = function(n) {
		return ((this%n)+n)%n;
	}	
	venues = {
		"250"  : "250",
		"500"   : "500",
		"1500"    : "1500",		
		"2000"    : "2000",		
		"2500"    : "2500",		
		"4000"    : "4000"
	};
	$(function() {
		var venueContainer = $('#venues ul');
		$.each(venues, function(key, item) {
			venueContainer.append(
		        $(document.createElement("li"))
		        .append(
	                $(document.createElement("input")).attr({
                         id:    'venue-' + key
                        ,name:  item
                        ,value: item
                        ,type:  'checkbox'
                        ,checked:true
	                })
	                .change( function() {
	                	var cbox = $(this)[0];
	                	var segments = wheel.segments;
	                	var i = segments.indexOf(cbox.value);
	                	if (cbox.checked && i == -1) {
	                		segments.push(cbox.value);
	                	} else if ( !cbox.checked && i != -1 ) {
	                		segments.splice(i, 1);
	                	}
	                	segments.sort();
	                	wheel.update();
	                } )
		        ).append(
	                $(document.createElement('label')).attr({
	                    'for':  'venue-' + key
	                })
	                .text( item )
		        )
		    )
		});
		$('#venues ul>li').tsort("input", {attr: "value"});
	});
	var me = 0;
	var wheel = {		
		timerHandle : 0,
		timerDelay : 33,
		angleCurrent : 0,
		angleDelta : 0,
		size : 200,
		canvasContext : null,
		colors : [ '#EC932B', '#C98EAA', '#E8E773', '#A0CA40', '#549FB2', '#DF547D'],
		segments : [],
		seg_colors : [], 
		maxSpeed : Math.PI / 16,
		upTime : 1000, 
		downTime : 20000, 
		spinStart : 0,
		frames : 0,
		centerX : 305,
		centerY : 295,
		spin : function() {						
			if (me <= 0){				
				if (wheel.timerHandle == 0) {
					wheel.spinStart = new Date().getTime();
					wheel.maxSpeed = Math.PI / (16 + Math.random()); // Randomly vary how hard the spin is
					wheel.frames = 0;
					wheel.sound.play();
					wheel.timerHandle = setInterval(wheel.onTimerTick, wheel.timerDelay);
				}
			}	
		},	
		onTimerTick : function() {
			wheel.frames++;
			wheel.draw();
			var duration = (new Date().getTime() - wheel.spinStart);
			var progress = 0;
			var finished = false;
			if (duration < wheel.upTime) {
				progress = duration / wheel.upTime;
				wheel.angleDelta = 	wheel.maxSpeed
									* Math.sin(progress * Math.PI / 2);
			} else {
				progress = duration / wheel.downTime;
				wheel.angleDelta = 	wheel.maxSpeed
									* Math.sin(progress * Math.PI / 2 + Math.PI / 2);
				if (progress >= 1)
					finished = true;					
			}
			wheel.angleCurrent += wheel.angleDelta;
			while (wheel.angleCurrent >= Math.PI * 2)				
				wheel.angleCurrent -= Math.PI * 2;
			if (finished) {							
				clearInterval(wheel.timerHandle);
				wheel.timerHandle = 0;
				wheel.angleDelta = 0;				
				var i = wheel.segments.length - Math.floor((wheel.angleCurrent / (Math.PI * 2))	* wheel.segments.length) - 1;
				me = 1;
				setInterval(function() {					 
					$("#wheel").fadeOut(500);									
				}, 2000);	

				/* $.ajax({
	                            type: "POST",
	                            url: '@Url.Action("Addchip","WagerBoard")',
								data: wheel.segments[i],



				                success: function (data) {
									
									alert("hiii");
				                  	
				                },
				            });
				*/
				$('#congratulations p').html('You Have Won <span>' + wheel.segments[i] + '</span> Chips');
				$("#congratulations").show(1000);
				setInterval(function() {					 					
					$('#congratulations').stop().animate({'top':110}, 1000);					
				}, 2000);
			}			
		},
		init : function(optionList) {
			try {
				wheel.initWheel();
				wheel.initAudio();
				wheel.initCanvas();
				wheel.draw();
				$.extend(wheel, optionList);
			} catch (exceptionData) {
				alert('Wheel is not loaded ' + exceptionData);
			}
		},
		initAudio : function() {
			var sound = document.createElement('audio');
			sound.setAttribute('src', 'Images/wheel.mp3');
			wheel.sound = sound;
		},
		initCanvas : function() {
			var canvas = $('#wheel #canvas').get(0);
			var canvass = $('#button').get(0);
			if ($.browser.msie) {
				canvas = document.createElement('canvas');
				$(canvas).attr('width', 500).attr('height', 400).attr('id', 'canvas').appendTo('.wheel');
				canvas = G_vmlCanvasManager.initElement(canvas);
			}						
			canvass.addEventListener("click", wheel.spin, false);
			wheel.canvasContext = canvas.getContext("2d");
		},
		initWheel : function() {
			wheel.colors;
		},
		update : function() {			
			var r = 0;
			wheel.angleCurrent = ((r + 0.5) / wheel.segments.length) * Math.PI * 2;
			var segments = wheel.segments;
			var len      = segments.length;
			var colors   = wheel.colors;
			var colorLen = colors.length;
			var seg_color = new Array();
			for (var i = 0; i < len; i++)
			seg_color.push( colors [i] );
			wheel.seg_color = seg_color;
			wheel.draw();
		},
		draw : function() {
			wheel.clear();
			wheel.drawWheel();
			wheel.drawNeedle();
		},
		clear : function() {
			var ctx = wheel.canvasContext;
			ctx.clearRect(0, 0, 1000, 800);
		},
		drawNeedle : function() {
			var ctx = wheel.canvasContext;
			var centerX = wheel.centerX;
			var centerY = wheel.centerY;
			var size = wheel.size;
			
			ctx.lineWidth = 1;
			ctx.strokeStyle = '#fff';
			ctx.fileStyle = '#ED1677';

			ctx.beginPath();

			ctx.moveTo(centerX + size - 40, centerY);
			ctx.lineTo(centerX + size + 20, centerY - 10);
			ctx.lineTo(centerX + size + 20, centerY + 10);
			ctx.closePath();
			/***********************dip*********************/
				/*ctx.stroke();
				ctx.fill();*/
			/***********************dip*********************/
						
			var i = wheel.segments.length - Math.floor((wheel.angleCurrent / (Math.PI * 2))	* wheel.segments.length) - 1;
			
			// Now draw the winning name
			ctx.textAlign = "left";
			ctx.textBaseline = "bold";
			ctx.fillStyle = '#fff';
			ctx.font = "25px Arial";
			var fill_text = wheel.segments[i] + " Chips";
			ctx.fillText(fill_text, 15 + size + 25, 563);						
		},
		drawSegment : function(key, lastAngle, angle) {
			var ctx = wheel.canvasContext;
			var centerX = wheel.centerX;
			var centerY = wheel.centerY;
			var size = wheel.size;

			var segments = wheel.segments;
			var len = wheel.segments.length;
			var colors = wheel.seg_color;
			
			var value = segments[key];
			
			ctx.save();
			ctx.beginPath();

			// Start in the centre
			ctx.moveTo(centerX, centerY);
			ctx.arc(centerX, centerY, size, lastAngle, angle, false); // Draw a arc around the edge
			ctx.lineTo(centerX, centerY); // Now draw a line back to the centre

			// Clip anything that follows to this area
			//ctx.clip(); // It would be best to clip, but we can double performance without it
			ctx.closePath();

			ctx.fillStyle = colors[key];
			ctx.fill();
			ctx.stroke();

			// Now draw the text
			ctx.save(); // The save ensures this works on Android devices
			ctx.translate(centerX, centerY);
			ctx.rotate((lastAngle + angle) / 2);

			ctx.fillStyle = '#000';
			ctx.fillText(value.substr(0, 20), size / 2 + 20, 0);
			ctx.restore();

			ctx.restore();
		},

		drawWheel : function() {
			var ctx = wheel.canvasContext;

			var angleCurrent = wheel.angleCurrent;
			var lastAngle    = angleCurrent;

			var segments  = wheel.segments;
			var len       = wheel.segments.length;
			var colors    = wheel.colors;
			var colorsLen = wheel.colors.length;

			var centerX = wheel.centerX;
			var centerY = wheel.centerY;
			var size    = wheel.size;

			var PI2 = Math.PI * 2;

			ctx.lineWidth    = 1;
			ctx.strokeStyle  = '#fff';
			ctx.textBaseline = "middle";
			ctx.textAlign    = "center";
			ctx.font         = "1.4em Arial";

			for (var i = 1; i <= len; i++) {
				var angle = PI2 * (i / len) + angleCurrent;
				wheel.drawSegment(i - 1, lastAngle, angle);
				lastAngle = angle;
			}
			// Draw a center circle
			ctx.beginPath();
			ctx.arc(centerX, centerY, 20, 0, PI2, false);
			ctx.closePath();

			ctx.fillStyle   = '#fff';
			ctx.strokeStyle = '#000';
			ctx.fill();
			ctx.stroke();

			// Draw outer circle
			ctx.beginPath();
			ctx.arc(centerX, centerY, size, 0, PI2, false);
			ctx.closePath();

			ctx.lineWidth   = 1;
			ctx.strokeStyle = '#fff';
			ctx.stroke();
		},
	}

	window.onload = function() {
		wheel.init();

		var segments = new Array();
		$.each($('#venues input:checked'), function(key, cbox) {
			segments.push( cbox.value );
		});

		wheel.segments = segments;
		wheel.update();

		// Hide the address bar (for mobile devices)!
		setTimeout(function() {
			window.scrollTo(0, 1);
		}, 0);
	}