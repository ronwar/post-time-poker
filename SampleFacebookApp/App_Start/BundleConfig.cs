﻿using System.Web;
using System.Web.Optimization;

namespace SampleFacebookApp
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
           bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                      "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                       "~/Scripts/jquery-ui-{version}.js"));

           // bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
             //           "~/Scripts/jquery.unobtrusive*",
               //         "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
         //   bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
           //             "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/jsjquery").Include("~/Scripts/jquery.js"));
           bundles.Add(new ScriptBundle("~/bundles/jsjquerymin").Include("~/Scripts/jquery.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jsfirst").Include("~/Scripts/jquery.liquidcarousel.pack.js"));
            bundles.Add(new ScriptBundle("~/bundles/jstwo").Include("~/Scripts/jquery.tools.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/jstabslider").Include("~/Scripts/jQuery.tab-slider.js"));
            bundles.Add(new ScriptBundle("~/bundles/jstool").Include("~/Scripts/jquery.tools.min.js"));
            
            
            bundles.Add(new StyleBundle("~/Content/csnormalize").Include("~/Content/normalize.css"));
            bundles.Add(new StyleBundle("~/Content/cssone").Include("~/Content/site.css"));
            bundles.Add(new StyleBundle("~/Content/csstabstyle").Include("~/Content/tab-slider-style.css"));
   
           
           

        
        //wager now bundle

            bundles.Add(new StyleBundle("~/bundles/wagerpad").Include("~/Content/tab-slider-style.css"));
        
        }
    }
}